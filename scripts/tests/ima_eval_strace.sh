#!/bin/bash

# This script evaluates the performance of xfilter extension
# by using strace to command `ls`

tmptestdir='/tmp/testdir'
tmpdir='/dev/shm'
tmptrfile="$tmpdir/trimmed"
tmpstracelog="$tmpdir/strace"

TESTCOUNT=100
[ -z $TRIMMED ] && TRIMMED=5
CPU_NUM=2
SCALE=6
[ -z $SLEEP ] && SLEEP=10

[ -z $NEED_MEAN_CALC ] && NEED_MEAN_CALC=1
[ -z $NEED_MEANS_TRIMMED_CALC ] && NEED_MEANS_TRIMMED_CALC=1
[ -z $MEAN_CALC_ONLY ] && MEAN_CALC_ONLY=0

trap 'clean' SIGINT
clean(){
  swapon -a
  [ -d $tmptestdir ] && rm -rf $tmptestdir
  [ -f $tmptrfile ] && rm -f $tmptrfile
  [ -f $tmpstracelog ] && rm -f $tmpstracelog
  rm -f "$tmpdir/$ima_status*.tmp"
  unset TIMEFORMAT TRIMMED NEED_MEAN_CALC NEED_MEANS_TRIMMED_CALC TRIMMED MEAN_CALC_ONLY
  EXITC=1
}

usage(){
  echo "Usage: $(basename $0) n"
}

gen_report_strace(){

  solve_log_exist $report_file

  for s in "${trace_call[@]}"; do
    log_header+=$(echo -e -n "$ima_status.$s,")
  done
  echo_record $log_header |tee -a ${report_file}

  for ((c=0; c < $TESTCOUNT; c++)); do 
    [ $EXITC ] && break
    [ ! -z $SLEEP ] && sleep $SLEEP
    cache_clean

    echo -n "$c "

    eval "strace -T $1 &> $tmpstracelog"

    duration[execve]=$(cat $tmpstracelog | \
        awk '/^execve.*\/usr\/bin\//{print $NF}' | \
            head -n1 | \
            sed 's/<\|>//g')
    duration[mmap]=$(cat $tmpstracelog | \
        awk '/^mmap.*NULL.*PROT_READ, MAP_PRIVATE, 3, 0/{print $NF}' | \
            head -n1 | \
            sed 's/<\|>//g')

    test $? -gt 128 && break

    for i in "${trace_call[@]}"; do
      [ -z "${duration[$i]}" ] && {
        echo "Not found: the duration of $i"
        [ ! -z $SLEEP ] && sleep $SLEEP
        continue 2
      }
    done

    unset log_entry
    for t in "${trace_call[@]}"; do
      log_entry+=$(echo -n -e "${duration[$t]},")
    done
    echo_record -n $log_entry |tee -a ${report_file}

    echo | tee -a ${report_file}
  done

} #gen_report_strace()


#######
# main
#######

source "$(dirname "$0")/ima_eval_include.sh"

[[ "$#" > 1 ]] && usage && exit
report_dir='./' 

test_cpu $CPU_NUM
test_dir $report_dir

test_ima_status 'strace'
testdir_prep $tmptestdir

[[ "$#" == 0 ]] &&  report_file="$report_dir/$ima_status.log"
[[ "$#" == 1 ]] &&  report_file="$report_dir/$ima_status.$1.log"

declare trace_call=( 'execve' 'mmap' )
declare -A duration=()

# Switch off the swap
swapoff -a

[[ $MEAN_CALC_ONLY != 1 ]] && \
    gen_report_strace 'ls -la $tmptestdir' && \
    draw_hl $report_file

[[ $NEED_MEAN_CALC == 1 ]]          && means         $report_file 2
[[ $NEED_MEANS_TRIMMED_CALC == 1 ]] && means_trimmed $report_file 2

clean
