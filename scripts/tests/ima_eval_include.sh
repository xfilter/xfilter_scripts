#!/bin/bash

# Common functions for all evaluation scripts

ima_policy='/sys/kernel/security/integrity/ima/policy'

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

TIMEFORMAT='%S'

test_dir() {
  [ ! -d $1 ] && {
    echo "Report directory does not available!"
    exit
  }
}

test_cpu() {
  [[ "$(grep -c '^processor\s\+:' /proc/cpuinfo)" != $1 ]] && {
    echo "Wrong CPU numbers, need to be $1"
    exit
  }
}

test_comm() {
  which $1
  [[ $? != '0' ]] && {
    echo "Test binary not found: $1"
    exit
  }
}

test_ima_status() {
  [[ $# != 1 ]] && echo "$0 arguments err" && exit

  [[ $(grep XF_XLIST $ima_policy) ]] && {
    xlist_c=$(cat /etc/sysconfig/ima-xlist |wc -l)
    ima_status="$1.ima_xlist.$xlist_c"
    return
  }

  [[ $(grep XF_XLABEL $ima_policy) ]] && {
    ima_status="$1.ima_xlabel"
    return
  }

  [[ $(grep 'measure.*func.*mask' $ima_policy) ]] && {
    ima_status="$1.ima_tcb"
    return
  }

  ima_status="$1.ima_disabled"

  return
}

testdir_prep(){
  [[ $# != 1 ]] && echo "$0 arguments err" && exit
  [ -d $1 ] && rm -rf $1
  mkdir $1
 
# Prepare some random files
  cat << EOF >  $1/donec
Donec lobortis ullamcorper euismod. Donec at pretium turpis. Nunc tellus sem,
gravida vel interdum quis, dapibus eu turpis. Etiam leo felis, porttitor in
nisl eu, cursus fringilla risus. Phasellus at arcu dignissim, malesuada nibh
vel, sagittis arcu.
EOF

  cat << EOF > $1/class
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
inceptos himenaeos. Vivamus viverra elit lorem, vitae posuere est consectetur
egestas. Integer varius interdum tortor, sed consectetur purus tristique id.
Donec ligula ante, pellentesque consequat dolor a.
EOF

  cat << EOF > $1/fusce
Fusce eget egestas arcu. Morbi eleifend semper venenatis. Ut consectetur quam
dolor, eget pretium orci semper at. Nulla facilisi. Sed rutrum sagittis urna,
sit amet pellentesque diam molestie at. Phasellus non hendrerit elit.
EOF

  cat << EOF > $1/lorem
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
vehicula cursus. Ut fringilla urna tellus, a dignissim mauris posuere eget.
Vestibulum non odio vel lectus dignissim scelerisque vel eget massa.
EOF

  cat << EOF > $1/pellentesque
Pellentesque tincidunt dui accumsan est tincidunt iaculis. Pellentesque tempus
ex leo, bibendum dapibus dui tempus ac. Proin posuere libero sit amet porta
elementum. Duis efficitur neque maximus est semper convallis.
EOF

  cat << EOF > $1/praesent
Praesent in orci ante. In nec gravida sem. Duis ac nibh tempor, pharetra magna
eu, lobortis lacus. Pellentesque at arcu ut massa facilisis interdum. Etiam nec
metus in sapien varius consequat. Phasellus non scelerisque orci.
EOF

  cat << EOF > $1/proin
Proin eu condimentum purus, id aliquam lorem. Nulla accumsan ornare quam, eu
tincidunt turpis tempor eget. Nullam suscipit dolor sit amet ex porta, pulvinar
euismod urna cursus. Duis in pharetra arcu, at euismod orci.
EOF

  cat << EOF > $1/suspendisse
Suspendisse sem urna, vehicula ac fringilla vitae, ullamcorper eu enim. Donec
ullamcorper iaculis est, vitae facilisis nisi feugiat eget.
EOF

  cat << EOF > $1/ut
Ut molestie, massa non lacinia dictum, dolor urna cursus arcu, in malesuada
nisi est quis ex.
EOF

}

round() {
  echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};

echo_record() {
  if [[ "$#" == 2 ]] && [[ "$1" == '-n' ]]; then
    echo -n $2 | sed 's/,$//'
  elif [[ "$#" == 1 ]]; then
    echo $1 | sed 's/,$//'
  else
    return 1
  fi
}


#######################################################
# Choose the actions if the report file already exists
# $1: File name
#######################################################
solve_log_exist() {
  [ -f $1 ] && {
    read -p "The log file $1 already exists! Overwirte? [Y/n]" -r REPLY 
    echo
    [ ! -z "$REPLY" ] && [[ ! $REPLY =~ ^[Yy]$ ]] && exit 1;
  }
  unset REPLY
  rm -f $1
}

#######################################################
# Draw horizontal line
# $1: File that contains all values
#######################################################
draw_hl() {
  echo -e "\n----------" |tee -a $1
}

cache_clean(){

# file system buffers
sync

# Page cache, dentry cache and inodes cache
echo 3 > /proc/sys/vm/drop_caches

}

#######################################################
# Calculate trimmed mean from the values in the file
# $1: File that contains all values
# $2: column - column numbers of data in the file
# External Variable: $TESTCOUNT, $TRIMMED, $SCALE,
#                    $tmptrfile
#######################################################

means_trimmed() {
  
  [[ "$#" != 2 ]] \
        && echo 'Error: means_trimmed() - argument numbers' \
        && exit
  [ ! -f $1 ] \
        && echo 'Error: means_trimmed() - log file not found' \
        && exit

  unset oneline oneline_skw oneline_krt

  for i in $(seq 1 $2); do
    cat $1 | \
    sed -n "2,$(( $TESTCOUNT + 1 ))p" | \
    awk -v c=$i -F ',' '{print $c}' | \
    sort -g | \
    awk -v s="$TRIMMED" \
        -v e="$(echo $TESTCOUNT - $TRIMMED|bc -l)" \
        'NR>s&&NR<=e' > $tmptrfile

    sum='0'
    while read p; do
      sum=$(echo "scale=$SCALE; $sum + $p" |bc -l)
    done < $tmptrfile
    avg=$(echo $(round "$sum / ($TESTCOUNT - $TRIMMED * 2)" $SCALE) |bc -l)

    skw="s$(pspp -O box=ascii ima_eval_pspp.sps |grep Skewness |awk -F '\|' '{print $3}' 2>/dev/null |sed 's/ //g')"
    krt="k$(pspp -O box=ascii ima_eval_pspp.sps |grep Kurtosis |awk -F '\|' '{print $3}' 2>/dev/null |sed 's/ //g')"

    oneline+="$avg, "
    oneline_skw+="$skw, "
    oneline_krt+="$krt, "
  done

  oneline=$(echo $oneline | sed s/,$//)
  oneline_skw=$(echo $oneline_skw | sed s/,$//)
  oneline_krt=$(echo $oneline_krt | sed s/,$//)

  echo -e "\nTruncated Means (-${TRIMMED}*2):" |tee -a $1
  echo -e $oneline |tee -a $1
  echo -e $oneline_skw |tee -a $1
  echo -e $oneline_krt |tee -a $1

} # means_trimmed()


#######################################################
# Calculate means from the values in the file
# $1: File that contains all values
# $2: column - column numbers of data in the file
#######################################################
means() {
  
  [[ "$#" != 2 ]] \
        && echo 'Error: mean() - argument numbers' \
        && exit
  [ ! -f $1 ] \
        && echo 'Error: mean() - log file not found' \
        && exit

  unset oneline oneline_skw oneline_krt

  for i in $(seq 1 $2); do
    cat $1 | \
    sed -n "2,$(( $TESTCOUNT + 1 ))p" | \
    awk -v c=$i -F ',' '{print $c}' | \
    sort -g > $tmptrfile

    sum='0'
    while read p; do
      sum=$(echo "scale=$SCALE; $sum + $p" |bc -l)
    done < $tmptrfile
    avg=$(echo $(round "$sum / $TESTCOUNT" $SCALE) |bc -l)

    skw="s$(pspp -O box=ascii ima_eval_pspp.sps |grep Skewness |awk -F '\|' '{print $3}' 2>/dev/null |sed 's/ //g')"
    krt="k$(pspp -O box=ascii ima_eval_pspp.sps |grep Kurtosis |awk -F '\|' '{print $3}' 2>/dev/null |sed 's/ //g')"

    oneline+="$avg, "
    oneline_skw+="$skw, "
    oneline_krt+="$krt, "
  done

  oneline=$(echo $oneline | sed s/,$//)
  oneline_skw=$(echo $oneline_skw | sed s/,$//)
  oneline_krt=$(echo $oneline_krt | sed s/,$//)

  echo -e "\nMeans:" |tee -a $1
  echo -e $oneline |tee -a $1
  echo -e $oneline_skw |tee -a $1
  echo -e $oneline_krt |tee -a $1

} # means()
