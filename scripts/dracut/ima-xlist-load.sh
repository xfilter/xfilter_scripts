#!/bin/sh

# Licensed under the GPLv2
#

IMASECDIR="${SECURITYFSDIR}/ima"
IMACONFIG="${NEWROOT}/etc/sysconfig/ima"
IMAXLIST="/etc/sysconfig/ima-xlist"

load_ima_xlist()
{
    # check kernel support for IMA
    if [ ! -e "${IMASECDIR}" ]; then
        if [ "${RD_DEBUG}" = "yes" ]; then
            info "integrity: IMA kernel support is disabled"
        fi
        return 0
    fi

    # set the IMA policy path name
    IMAXLISTPATH="${NEWROOT}${IMAXLIST}"

    # check the existence of the IMA xlist file
    [ -f "${IMAPOLICYPATH}" ] && {
        info "Loading the provided IMA xlist";
        printf '%s' "${IMAXLISTPATH}" > ${IMASECDIR}/xlist || \
            cat "${IMAXLISTPATH}" > ${IMASECDIR}/xlist
    }

    return 0
}

load_ima_xlist
